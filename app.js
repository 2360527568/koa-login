// node自带模块
const path = require('path')

// mongoose
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/login_sample', { useNewUrlParser: true })

// koa
const Koa = require('koa')
const app = new Koa()
const render = require('koa-art-template')
const bodyParser = require('koa-bodyparser')

const login = require('./routes/account/login')
const register = require('./routes/account/register')

// 静态资源
app.use(require('koa-static')(
    path.join(__dirname, 'statics')
))

// 请求body解析器
app.use(bodyParser())

// 模板引擎
render(app, {
    root: path.join(__dirname, 'views'),
    extname: '.html',
    debug: process.env.NODE_ENV !== 'production'
})

// 访问记录
app.use((ctx, next) => {
    console.log(ctx.method, ctx.url, ctx.host, new Date().toLocaleString())
    next()
})

// 路由
app.use(login.routes(), login.allowedMethods())
app.use(register.routes(), register.allowedMethods())

app.listen(3000)
console.clear()
console.log(app)