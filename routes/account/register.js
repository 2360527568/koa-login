const router = require('koa-router')()
const userModel = require('../../models/user')

// ==== 页面 ====
router.get('/register', async ctx => {
    await ctx.render('account/register', {
        regUrl: '/register'
    })
})

// ==== 接口 ====
router.post('/usernameCheck', async ctx => {
    if (ctx.request.body.username) {
        ctx.body = '用户已存在' || await userModel.findOne({ username: ctx.request.body.username })
    }
})

router.post('/emailCheck', async ctx => {
    if (ctx.request.body.email) {
        ctx.body = '邮箱已注册' || await userModel.findOne({ email: ctx.request.body.email })
    }
})

router.post('/register', async ctx => {
    for (item in ctx.request.body) {
        if (!ctx.request.body[item]) {
            ctx.body = `${item} 必填`
            return
        }
    }
    userModel.create({
        username: ctx.request.body.username,
        email: ctx.request.body.email,
        password: ctx.request.body.password
    })
    ctx.body = '注册成功'
})

module.exports = router