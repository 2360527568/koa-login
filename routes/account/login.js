const router = require('koa-router')()
const userModel = require('../../models/user')

router.get('/login', async ctx => {
    await ctx.render('login')
})

module.exports = router